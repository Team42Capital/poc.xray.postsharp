using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Amazon.XRay.Recorder.Core;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace AdminDashboard.Pages
{
    [Authorize]
    public class AboutModel : PageModel
    {
        public void OnGet()
        {
            var toto = TaMere(42.0);
        }

        public double TaMere(double x)
        {
            return x * x;
        }
    }
}
