﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using Amazon.XRay.Recorder.Core;
using PostSharp.Aspects;
using PostSharp.Extensibility;
using PostSharp.Serialization;

namespace POX.XRay.Postsharp2
{
    [MulticastAttributeUsage(MulticastTargets.Method,
        TargetMemberAttributes = MulticastAttributes.Instance)]
    [AttributeUsage(AttributeTargets.Assembly |
                    AttributeTargets.Class |
                    AttributeTargets.Method, AllowMultiple = true)]
    [PSerializable]
    public class XRayTracerAspect : OnMethodBoundaryAspect
    {
        public override void OnEntry(MethodExecutionArgs args)
        {
            try
            {
                AWSXRayRecorder.Instance.BeginSubsegment(args.Method.Name);

            //SM: trying to log function arguments. Unseccessfull. Keep trying
            //    if (args.Arguments?.Count > 0)
            //    {
            //        try
            //        {
            //            //AWSXRayRecorder.Instance.AddAnnotation("Args", JsonSerializer.Serialize(args.Arguments.ToArray()));
            //            //AWSXRayRecorder.Instance.AddAnnotation("NbArgs", args.Arguments.Count);
            //            var aArgs = args.Arguments.ToArray();
            //            for(var i=0;i<args.Arguments.Count;++i)
            //            {
            //                var arg = aArgs[i];
            //                //AWSXRayRecorder.Instance.AddAnnotation(i+arg?.GetType()?.ToString(), arg?.ToString());
            //            }
                        
            //        }
            //        catch (Exception e)
            //        {
            //            //AWSXRayRecorder.Instance.AddException(e);
            //        }

            //    }
            }
            catch (Exception e)
            {
            }

        }

        public override void OnExit(MethodExecutionArgs args)
        {
            try
            {
                AWSXRayRecorder.Instance.EndSubsegment();
            }
            catch (Exception e)
            {
            }

            //SM: trying to log the result. Unseccessfull. Keep trying
            //try
            //{
            //    //if (args.ReturnValue != null)
            //    //    AWSXRayRecorder.Instance.AddAnnotation("Result", JsonSerializer.Serialize(args.ReturnValue));

            //}
            //catch (Exception e)
            //{
            //    try
            //    {
            //        //AWSXRayRecorder.Instance.AddException(e);
            //    }
            //    catch (Exception exception)
            //    {
            //    }

            //}
            //finally
            //{
            //    //AWSXRayRecorder.Instance.EndSubsegment();
            //}
        }

        public override void OnException(MethodExecutionArgs args)
        {
            try
            {
                AWSXRayRecorder.Instance.AddException(args.Exception);
                AWSXRayRecorder.Instance.MarkFault();

            }
            catch (Exception e)
            {
            }
        }
    }

}
