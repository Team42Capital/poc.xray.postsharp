﻿// See https://aka.ms/new-console-template for more information

using Amazon.XRay.Recorder.Core;
using hg.Common.Lib;
using hg.Server;
using hg.XRay.Postsharp;
using Microsoft.Extensions.Configuration;
using PostSharp.Extensibility;
using Serilog;

[assembly: hgXrayBaseAspect(AttributePriority = 1,
    AttributeTargetMemberAttributes = MulticastAttributes.Protected |
                                      MulticastAttributes.Internal |
                                      MulticastAttributes.Private |
                                      MulticastAttributes.Public)]

Log.Logger = new LoggerConfiguration()
    .WriteTo.Async(c => c.Console())
    .MinimumLevel.Verbose()
    .CreateLogger();

Log.Information("Starting POC Server");
Log.Information("Processing jobs");

var config = new ConfigurationBuilder()
    .AddJsonFile("appsettings.json", optional: false)
    .Build();
AWSXRayRecorder.InitializeInstance(config);

var locker = new object();
var tasks = Enumerable.Range(0, 10).Select(i => Task.Run(() =>
{
    var iProc = i;
    var redis = new hgRedis();
    var server = new hgServer();
    while (true)
    {
        lock (locker)
        {
            var req = redis.PopRequest();
            if (req == null)
            {
                Log.Debug($"Processor {iProc}: no request to process");
                Thread.Sleep(100);
                return;
            }
            Log.Debug($"Processor {iProc}: processing request {req.ID}");
            try
            {
                server.ProcessRequest(req);
            }
            catch (Exception e)
            {
            }
            
        }
    }
})).ToArray();


Task.WaitAll(tasks);