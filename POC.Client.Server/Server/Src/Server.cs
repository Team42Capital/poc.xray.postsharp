﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using hg.Common.Lib;

namespace hg.Server
{
    public class hgServer
    {
        #region Properties

        public double FailureRate { get; set; } = 0.05;
        public double CascadeProba { get; set; } = 0.25;
        public Random RandomGen { get; } = new Random();
        #endregion
        #region Public functions

        public void ProcessRequest(Request req)
        {
            switch (req.RequestType)
            {
                case EReqType.Type1:
                    DoSomething1(req.Argument1, req.Argument2, req.CustomerID);
                    break;
                case EReqType.Type2:
                    DoSomething2(req.Argument1, req.Argument2, req.CustomerID);
                    break;
                case EReqType.Type3:
                    DoSomething3(req.Argument1, req.Argument2, req.CustomerID);
                    break;
                default:
                    throw new hgDummyException();
            }
        }
        public void DoSomething1(string arg1, int arg2, string customerID)
        {
            var ran = this.RandomGen.NextDouble();
            if (ran < this.FailureRate)
                throw new hgDummyException();

        }

        public void DoSomething2(string arg1, int arg2, string customerID)
        {
            var ran = this.RandomGen.NextDouble();
            if (ran < this.FailureRate)
                throw new hgDummyException();

            if (this.RandomGen.NextDouble() < this.CascadeProba)
            {
                var req1 = Request.CreateRequest(arg1, arg2, customerID, EReqType.Type1);
            }
            
            //var req2 = Request.CreateRequest(arg1, arg2, customerID, EReqType.Type1);
            //var req3 = Request.CreateRequest(arg1, arg2, customerID, EReqType.Type1);
        }

        public void DoSomething3(string arg1, int arg2, string customerID)
        {
            var ran = this.RandomGen.NextDouble();
            if (ran < this.FailureRate)
                throw new hgDummyException();

            if (this.RandomGen.NextDouble() < this.CascadeProba)
            {
                var req1 = Request.CreateRequest(arg1, arg2, customerID, EReqType.Type1);
            }

            //var req2 = Request.CreateRequest(arg1, arg2, customerID, EReqType.Type2);
        }


        #endregion

        #region Private Methods

        #endregion
    }
}
