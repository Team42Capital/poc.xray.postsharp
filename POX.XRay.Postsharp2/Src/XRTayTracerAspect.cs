﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Amazon.XRay.Recorder.Core;
using PostSharp.Aspects;
using PostSharp.Extensibility;
using PostSharp.Serialization;

namespace POX.XRay.Postsharp2
{
    [MulticastAttributeUsage(MulticastTargets.Method,
        TargetMemberAttributes = MulticastAttributes.Instance)]
    [AttributeUsage(AttributeTargets.Assembly |
                    AttributeTargets.Class |
                    AttributeTargets.Method, AllowMultiple = true)]
    [PSerializable]
    public class XRayTracerAspect : OnMethodBoundaryAspect
    {
        public override void OnEntry(MethodExecutionArgs args)
        {
#if FALSE
            Console.WriteLine($"Method <{args.Method.Name}> Started at {DateTime.Now:HH:mm:ss.fff}");
#else
            try
            {
                AWSXRayRecorder.Instance.BeginSubsegment(args.Method.Name);
            }
            catch (Exception e)
            {
            }
#endif
        }

        public override void OnExit(MethodExecutionArgs args)
        {
#if FALSE
            Console.WriteLine($"Method <{args.Method.Name}> Terminated at {DateTime.Now:HH:mm:ss.fff}");
#else
            try
            {
                AWSXRayRecorder.Instance.EndSubsegment();
            }
            catch (Exception e)
            {
            }
#endif
        }

        public override void OnException(MethodExecutionArgs args)
        {
#if FALSE
            Console.WriteLine($"Method <{args.Method.Name}> raised exception: <{args.Exception.Message}");
#else
            try
            {
                AWSXRayRecorder.Instance.AddException(args.Exception);
            }
            catch (Exception e)
            {
            }
#endif
        }
    }

}
