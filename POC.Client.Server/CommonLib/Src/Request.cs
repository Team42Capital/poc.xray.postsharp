﻿using hgXRayPostsharp;

namespace hg.Common.Lib
{
    public class Request
    {
        #region Properties
        public Guid ID { get; } = Guid.NewGuid();
        public string Argument1 { get; set; }
        public int Argument2 { get; set; }
        public string TraceID { get; set; }
        public string CustomerID { get; set; }

        public EReqType RequestType { get; set; }

        #endregion

        #region Static Functions

        public static Request CreateRequest(string arg1, int arg2, string customerID, EReqType reqType, string traceID = null)
        {
            var req = new Request
            {
                Argument1 = arg1,
                Argument2 = arg2,
                CustomerID = customerID,
                TraceID = string.IsNullOrEmpty(traceID) ? hgXRay.CreateTraceId() : traceID,
                RequestType = reqType
            };

            var redis = new hgRedis();
            redis.SendRequest(req);

            return req;
        }
        #endregion
    }
}