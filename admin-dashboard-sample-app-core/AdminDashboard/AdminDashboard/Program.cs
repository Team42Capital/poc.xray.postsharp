using AdminDashboard.Data;
using AdminDashboard.Data.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Amazon;
using Amazon.XRay.Recorder.Core;
using log4net;
using log4net.Config;
using PostSharp.Extensibility;
using POX.XRay.Postsharp2;

[assembly: XRayTracerAspect(AttributePriority = 1,
    AttributeTargetMemberAttributes = MulticastAttributes.Protected |
                                      MulticastAttributes.Internal |
                                      MulticastAttributes.Private |
                                      MulticastAttributes.Public)]

namespace AdminDashboard
{
    public class Program
    {
        private static ILog log;
        public static void Main(string[] args)
        {
            //CreateHostBuilder(args).Build().Run();
            var logRepository = LogManager.GetRepository(Assembly.GetEntryAssembly());
            XmlConfigurator.Configure(logRepository, new FileInfo("log4net.config"));
            log = LogManager.GetLogger(typeof(Program));
            AWSXRayRecorder.RegisterLogger(LoggingOptions.Log4Net);

            var host = CreateHostBuilder(args).Build();

            CreateDbIfNotExists(host);
            host.Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });

        private static async void CreateDbIfNotExists(IHost host)
        {
            using (var scope = host.Services.CreateScope())
            {
                var services = scope.ServiceProvider;

                try
                {
                    var context = services.GetRequiredService<ApplicationDbContext>();
                    var environment = services.GetRequiredService<IWebHostEnvironment>();
                    UserManager<ApplicationUser> userManager = services.GetRequiredService<UserManager<ApplicationUser>>();
                    context.Database.Migrate();
                    await DbInitializer.Initialize(context, environment, userManager);
                }
                catch (Exception ex)
                {
                    var logger = services.GetRequiredService<ILogger<Program>>();
                    logger.LogError(ex, "An error occurred creating the DB.");
                }
            }
        }
    }
}
