using System;

using Amazon.Lambda.Core;
using Amazon.XRay.Recorder.Core;
using Amazon.XRay.Recorder.Handlers.AwsSdk;
using Newtonsoft.Json;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.SystemTextJson.DefaultLambdaJsonSerializer))]

namespace POC.XRay.Lambda
{
    public class Function
    {
        static Function()
        {
            initialize();
        }

        static async void initialize()
        {
            AWSSDKHandler.RegisterXRayForAllServices();
        }

        /// <summary>
        /// A simple function that takes a string and does a ToUpper
        /// </summary>
        /// <param name="input"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public string FunctionHandler(string input, ILambdaContext context)
        {
            var recorder = new AWSXRayRecorder();
            recorder.BeginSubsegment("UpperCase");
            recorder.BeginSubsegment("Inner 1");
            String result = input?.ToUpper();
            recorder.EndSubsegment();
            recorder.BeginSubsegment("Inner 2");
            recorder.EndSubsegment();
            recorder.EndSubsegment();

            LambdaLogger.Log("ENVIRONMENT VARIABLES: " + JsonConvert.SerializeObject(System.Environment.GetEnvironmentVariables()));
            LambdaLogger.Log("CONTEXT: " + JsonConvert.SerializeObject(context));
            LambdaLogger.Log("INPUT: " + input);
            return result;
        }


    }
}
