﻿// See https://aka.ms/new-console-template for more information


using Amazon.XRay.Recorder.Core;
using hg.Client;
using hg.XRay.Postsharp;
using Microsoft.Extensions.Configuration;
using PostSharp.Extensibility;
using Serilog;

[assembly: hgXrayBaseAspect(AttributePriority = 1,
    AttributeTargetMemberAttributes = MulticastAttributes.Protected |
                                      MulticastAttributes.Internal |
                                      MulticastAttributes.Private |
                                      MulticastAttributes.Public)]

Log.Logger = new LoggerConfiguration()
    .WriteTo.Async(c=>c.Console())
    .MinimumLevel.Verbose()
    .CreateLogger();

var config = new ConfigurationBuilder()
    .AddJsonFile("appsettings.json", optional: false)
    .Build();
AWSXRayRecorder.InitializeInstance(config);

Log.Information("Starting POC Client");
Log.Information("Generate jobs");

var client = new hgClient();
var sleep = TimeSpan.FromMilliseconds(1);

while (true)
{
    var req = client.DoSomething();

    Thread.Sleep(sleep);
}


