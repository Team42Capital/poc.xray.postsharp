﻿using PostSharp;
using PostSharp.Extensibility;
using POX.XRay.Postsharp2;

[assembly: XRayTracerAspect(AttributePriority = 1,
    AttributeTargetMemberAttributes = MulticastAttributes.Protected | 
                                      MulticastAttributes.Internal |
                                      MulticastAttributes.Private |
                                      MulticastAttributes.Public)]
