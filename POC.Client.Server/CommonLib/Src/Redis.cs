﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Serilog;
using StackExchange.Redis;

namespace hg.Common.Lib
{
    public class hgRedis
    {
        #region Properties

        public string RedisConnectionString { get; set; } = "localhost";
        public string RedisChannelName { get; set; } = "JobChannel";
        public string RedisQueuelName { get; set; } = "JobQueue";


        #endregion
        #region Public Methods



        public void SendRequest(Request req)
        {
            var db = this.GetDatabase();


            //db.StringSet(req.ID.ToString(), JsonSerializer.Serialize(req));
            try
            {
                db.ListRightPush(this.RedisQueuelName, JsonSerializer.Serialize(req));
            }
            catch (Exception e)
            {
                Log.Error(e.ToString());
            }
            
        }

        public Request PopRequest()
        {

            try
            {
                var db = this.GetDatabase();
                var value = db.ListLeftPop(this.RedisQueuelName);
                if (value.IsNullOrEmpty)
                    return null;

                var req = JsonSerializer.Deserialize<Request>(value.ToString());
                return req;
            }
            catch (Exception e)
            {
                Log.Error(e.ToString());
            }

            return null;
        }

        #endregion

        #region Private Methods

        private ConnectionMultiplexer GetConnection()
        {
            var options = new ConfigurationOptions
            {
                AbortOnConnectFail = false,
                EndPoints = { this.RedisConnectionString }
            };
            var res = ConnectionMultiplexer.Connect(options);
            return res;
        }

        private IDatabase GetDatabase()
        {
            var conn = this.GetConnection();
            var db  = conn.GetDatabase();

            return db;
        }
        

        #endregion
    }
}
