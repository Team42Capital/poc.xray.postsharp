﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Amazon.XRay.Recorder.Core;

namespace hgXRayPostsharp
{
    public class hgXRay
    {
        #region Fields

        #endregion

        #region Properties

        #endregion

        #region Methods

        void OpenSegment(string name, string parentName="", string traceId="")
        {
            AWSXRayRecorder.Instance.BeginSegment(name, traceId:traceId, parentId:parentName);
        }

        void CloseSegment(string name)
        {
            AWSXRayRecorder.Instance.EndSegment();
        }

        void AddAnnotation(string key, object value)
        {
            AWSXRayRecorder.Instance.AddAnnotation(key, value);
        }

        void AddMetadata(string key, object value)
        {
            AWSXRayRecorder.Instance.AddMetadata(key, value);
        }

        public static string CreateTraceId()
        {
            var id = new Guid();
            var res = id.ToString();
            return res;
        }

        #endregion
    }
}
