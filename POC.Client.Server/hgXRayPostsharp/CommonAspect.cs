﻿using Amazon.XRay.Recorder.Core;
using PostSharp.Aspects;
using PostSharp.Extensibility;
using PostSharp.Serialization;
using Serilog;

namespace hg.XRay.Postsharp
{
    [MulticastAttributeUsage(MulticastTargets.Method,
        TargetMemberAttributes = MulticastAttributes.Instance)]
    [AttributeUsage(AttributeTargets.Assembly |
                    AttributeTargets.Class |
                    AttributeTargets.Method, AllowMultiple = true)]
    [PSerializable]
    public class hgXrayBaseAspect : OnMethodBoundaryAspect
    {
        public override void OnEntry(MethodExecutionArgs args)
        {
            try
            {
                //Log.Debug($"OnEntry:{args.Method.Name}");
                AWSXRayRecorder.Instance.BeginSubsegment(args.Method.Name);
            }
            catch (Exception e)
            {
            }

        }

        public override void OnExit(MethodExecutionArgs args)
        {
            try
            {
                AWSXRayRecorder.Instance.EndSubsegment();
            }
            catch (Exception e)
            {
            }

        }

        public override void OnException(MethodExecutionArgs args)
        {
            try
            {
                AWSXRayRecorder.Instance.AddException(args.Exception);
                AWSXRayRecorder.Instance.MarkFault();

            }
            catch (Exception e)
            {
            }
        }
    }
}