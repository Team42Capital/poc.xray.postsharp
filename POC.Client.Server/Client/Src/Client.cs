﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Amazon.XRay.Recorder.Core;
using hg.Common.Lib;
using hgXRayPostsharp;
using Serilog;

namespace hg.Client
{
    public class hgClient
    {
        #region Fields

        private Random m_random = new Random();
        

        #endregion
        public Request DoSomething()
        {
            AWSXRayRecorder.Instance.BeginSegment("Client");
            AWSXRayRecorder.Instance.BeginSubsegment("DoSomething");
            var arg1 = Guid.NewGuid().ToString();
            var arg2 = m_random.Next();
            var customerID = "Client." + m_random.Next(100);
            var reqType = (EReqType)(m_random.Next(2) + 1);

            var req = DoSomething(arg1, arg2, customerID, reqType);
            AWSXRayRecorder.Instance.EndSubsegment();
            AWSXRayRecorder.Instance.EndSegment();
            
            return req;
        }
        public Request DoSomething(string arg1, int arg2, string customerID, EReqType reqType)
        {
            var req = Request.CreateRequest(arg1, arg2, customerID, reqType);

            Log.Debug($"Client: sending request {req.ID}");
            return req;
        }





    }
}
